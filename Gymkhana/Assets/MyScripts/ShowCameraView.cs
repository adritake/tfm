﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCameraView : MonoBehaviour
{


	[Header("Camera")]
	public GameObject camera;

    // Start is called before the first frame update
    void Start()
    {
		var devices = WebCamTexture.devices;
		var backCamName = "";
		if(devices.Length > 0) backCamName = devices[0].name;
		for(int i = 0; i < devices.Length; i++) {
			Debug.Log("Device:" + devices[i].name + "IS FRONT FACING:" + devices[i].isFrontFacing);
			if(!devices[i].isFrontFacing) {
				backCamName = devices[i].name;
			}
		}
		var CameraTexture = new WebCamTexture(backCamName, 10000, 10000, 30);
		CameraTexture.Play();
		var renderer = GetComponent<Renderer>();
		renderer.material.mainTexture = CameraTexture;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
