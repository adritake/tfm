﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

	public GameObject trigger;
	public GameObject player;

	private Rigidbody rb;
	private bool flying;

	// Start is called before the first frame update
    void Start()
    {
		rb = GetComponent<Rigidbody> ();
		flying = false;
    }

    // Update is called once per frame
    void Update()
    {
		float dist = Vector3.Distance(player.transform.position, trigger.transform.position);
		if (dist < 1.0f && !flying) {
			rb.velocity = BallisticVel (player.transform, 60.0f);
			flying = true;
		}
    }

	private Vector3 BallisticVel (Transform target, float angle) {
		var dir = target.position - transform.position;  // get target direction
		var h = dir.y;  // get height difference
		dir.y = 0;  // retain only the horizontal direction
		var dist = dir.magnitude ;  // get horizontal distance
		var a = angle * Mathf.Deg2Rad;  // convert angle to radians
		dir.y = dist * Mathf.Tan(a);  // set dir to the elevation angle
		dist += h / Mathf.Tan(a);  // correct for small height differences
		// calculate the velocity magnitude
		var vel = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a));
		return vel * dir.normalized;
	}

	
}
