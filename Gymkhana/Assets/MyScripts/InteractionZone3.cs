﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionZone3 : MonoBehaviour
{

    [Header("Player")]
    public GameObject player;
    [Header("Start point")]
    public GameObject start;
    [Header("Finish point")]
    public GameObject end;


    private double alfa = 0.0f;


    // Start is called before the first frame update
    void Start()
    {
        float x1 = start.transform.position[0];
        float z1 = start.transform.position[2];

        float x2 = player.transform.position[0];
        float z2 = player.transform.position[2];

        float a = Math.Abs(x2 - x1);
        float c = Math.Abs(z2 - z1);

        alfa = Math.Atan(a / c);
        alfa = alfa * (180.0 / Math.PI);
		alfa += 270;
        //Debug.Log("ALFA = " + alfa);


    }

    // Update is called once per frame
    void Update()
    {
        /*
        Transform child = player.transform.Find("Controller (right)");
        //Debug.Log(child.rotation.eulerAngles[0] + " " + child.rotation.eulerAngles[1] + " " + child.rotation.eulerAngles[2]);
        double beta = child.rotation.eulerAngles[1];

        if (Math.Abs((360.0-alfa) - beta) < 5)
            Debug.Log("SIIIII");
            */

		//checkOrientation();

    }


    public void checkOrientation()
    {

        float dist = Vector3.Distance(player.transform.position, end.transform.position);

        if(dist < 3.0f)
        {
            //Debug.Log("ALFA = " + alfa);
            Transform child = player.transform.Find("Controller (right)");
            double beta = child.rotation.eulerAngles[1];

			if (beta > 270) {
				double error = Math.Abs (alfa - beta);

				double precision = ((error * -100) / 50) + 100;
				Debug.Log ("Precision en zona 3 = " + precision + "%");
			} else
				Debug.Log ("Mala precisión (angulo = " + beta + " ) ");

        }
    }
}
