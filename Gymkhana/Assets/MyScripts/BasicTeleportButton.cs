﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicTeleportButton : MonoBehaviour
{

    private bool enabled;
    private Color myColor;
    private Renderer myRenderer;

    private VRTK.VRTK_BasicTeleport basicTeleport;
    private VRTK.VRTK_DashTeleport dashTeleport;

    [Header("Game object with the scripts of teleporting")]
    public GameObject scriptHolder;

    // Start is called before the first frame update
    void Start()
    {
        enabled = false;
        myColor = Color.red;
        myRenderer = GetComponent<Renderer>();
        myRenderer.material.color = myColor;

        basicTeleport = scriptHolder.GetComponent<VRTK.VRTK_BasicTeleport>();
        dashTeleport = scriptHolder.GetComponent<VRTK.VRTK_DashTeleport>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnCollisionEnter(Collision col)
    {


        if (enabled)
        {
            enabled = false;
            myColor = Color.red;
            myRenderer.material.color = myColor;

            if (basicTeleport != null)
                basicTeleport.transform.gameObject.SetActive(false);

            if (dashTeleport != null)
                basicTeleport.transform.gameObject.SetActive(false);

        }
        else
        {
            enabled = true;
            myColor = Color.green;
            myRenderer.material.color = myColor;
        }

        
    }
}
