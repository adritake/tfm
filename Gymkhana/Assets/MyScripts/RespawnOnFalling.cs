﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnOnFalling : MonoBehaviour
{

    [Header("Player")]
    public GameObject player;

    [Header("Respawn point")]
    public GameObject respawn;


    private CapsuleCollider playerCollider;

    // Start is called before the first frame update
    void Start()
    {
        playerCollider = GameObject.Find("[VRTK][AUTOGEN][BodyColliderContainer]").GetComponent<CapsuleCollider>();
    }


    void OnTriggerEnter(Collider other)
    {
        if (other == playerCollider)
        {
            player.transform.position = respawn.transform.position;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other == playerCollider)
        {
            player.transform.position = respawn.transform.position;
        }
    }
}
