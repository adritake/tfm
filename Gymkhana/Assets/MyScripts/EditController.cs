﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditController : MonoBehaviour {


	public VRTK.VRTK_Pointer pointer;

	public virtual void setStraightRenderer(VRTK.VRTK_StraightPointerRenderer renderer){

		pointer.pointerRenderer = renderer;

	}

	public virtual void setBeziertRenderer(VRTK.VRTK_BezierPointerRenderer renderer){

		pointer.pointerRenderer = renderer;

	}
}
