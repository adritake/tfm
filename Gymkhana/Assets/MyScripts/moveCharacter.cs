﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveCharacter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, -4f, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, 4f, 0);

        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.position += transform.forward * (float)0.1;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            this.transform.position -= transform.forward * (float)0.1;
        }

        if (Input.GetKey(KeyCode.Space ))
        {
            Vector3 position = this.transform.position;
            position.y += (float)0.1;
            this.transform.position = position;
        }

        if (Input.GetKey(KeyCode.LeftControl))
        {
            Vector3 position = this.transform.position;
            position.y -= (float)0.1;
            this.transform.position = position;
        }
    }
}
