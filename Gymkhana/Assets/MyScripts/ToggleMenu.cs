﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleMenu : MonoBehaviour
{

    private bool active = true;

    public GameObject leftController;
    public GameObject rightController;


   public void toggleActive()
    {
        active = !active;
        this.transform.gameObject.SetActive(active);

        bool isBezier = rightController.GetComponent<VRTK.VRTK_BezierPointerRenderer>().enabled;

        if (isBezier && active)
        {
            leftController.GetComponent<VRTK.VRTK_BezierPointerRenderer>().enabled = false;
            rightController.GetComponent<VRTK.VRTK_BezierPointerRenderer>().enabled = false;

            leftController.GetComponent<VRTK.VRTK_StraightPointerRenderer>().enabled = true;
            rightController.GetComponent<VRTK.VRTK_StraightPointerRenderer>().enabled = true;

            leftController.GetComponent<VRTK.VRTK_Pointer>().pointerRenderer = leftController.GetComponent<VRTK.VRTK_StraightPointerRenderer>();
            rightController.GetComponent<VRTK.VRTK_Pointer>().pointerRenderer = rightController.GetComponent<VRTK.VRTK_StraightPointerRenderer>();
        }
    }
}
