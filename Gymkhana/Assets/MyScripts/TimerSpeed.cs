﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerSpeed : MonoBehaviour
{

	[Header("Zone 1")]
	public GameObject start1;
	//public GameObject end1;
	[Header("Zone 2")]
	public GameObject start2;
	//public GameObject end2;
	[Header("Zone 3")]
	public GameObject start3;
	//public GameObject end3;
	[Header("Zone 4")]
	public GameObject start4;
	//public GameObject end4;
	[Header("Zone 5")]
	public GameObject start5;
	//public GameObject end5;
	[Header("Zone 6")]
	public GameObject start6;
	//public GameObject end6;
	[Header("Zone 7")]
	public GameObject start7;
	public GameObject end7;

	[Header("Player")]
	public GameObject player;


	private bool [] inZones;
	private float [] times;

	//Trigger distance
	private float td = 3.0f;

	private float current_time;
	private float start_time;

	// Start is called before the first frame update
	void Start()
	{
		start_time = current_time = Time.time;
		inZones = new bool[] {true, false, false, false, false, false, false, false};
		times = new float[] {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	}

	void OnEnable()
	{
		start_time = current_time = Time.time;
		inZones = new bool[] {true, false, false, false, false, false, false, false};
		times = new float[] {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
	}

	// Update is called once per frame
	void Update()
	{

		if (inZones [0]) {
			float dist = Vector3.Distance (player.transform.position, start1.transform.position);

			if (dist < td) {
				inZones [0] = false;
				inZones [1] = true;
				times [0] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en zona 0 = " + times [0]);
			}

		} else if (inZones [1]) {

			float dist = Vector3.Distance (player.transform.position, start2.transform.position);

			if (dist < td) {
				inZones [1] = false;
				inZones [2] = true;
				times [1] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en zona 1 = " + times [1]);
			}

		}
		else if (inZones [2]) {

			float dist = Vector3.Distance (player.transform.position, start3.transform.position);

			if (dist < td) {
				inZones [2] = false;
				inZones [3] = true;
				times [2] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en zona 2 = " + times [2]);
			}

		}
		else if (inZones [3]) {

			float dist = Vector3.Distance (player.transform.position, start4.transform.position);

			if (dist < td) {
				inZones [3] = false;
				inZones [4] = true;
				times [3] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en zona 3 = " + times [3]);
			}

		}
		else if (inZones [4]) {

			float dist = Vector3.Distance (player.transform.position, start5.transform.position);

			if (dist < td) {
				inZones [4] = false;
				inZones [5] = true;
				times [4] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en zona 4 = " + times [4]);
			}

		}
		else if (inZones [5]) {

			float dist = Vector3.Distance (player.transform.position, start6.transform.position);

			if (dist < td) {
				inZones [5] = false;
				inZones [6] = true;
				times [5] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en zona 5 = " + times [5]);
			}

		}
		else if (inZones [6]) {

			float dist = Vector3.Distance (player.transform.position, start7.transform.position);

			if (dist < td) {
				inZones [6] = false;
				inZones [7] = true;
				times [6] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en zona 6 = " + times [6]);
			}

		}
		else if (inZones [7]) {

			float dist = Vector3.Distance (player.transform.position, end7.transform.position);

			if (dist < td) {
				inZones [7] = false;
				inZones [0] = true;
				times [7] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en zona 7 = " + times [7]);
				Debug.Log ("TIEMPO TOTAL = " + (Time.time - start_time));
			}

		}
	
	}
}

