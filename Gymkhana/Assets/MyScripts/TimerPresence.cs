﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerPresence : MonoBehaviour
{

	[Header("Baliza 1")]
	public GameObject baliza1;

	[Header("Baliza 2")]
	public GameObject baliza2;

	[Header("Baliza 3")]
	public GameObject baliza3;

	[Header("Player")]
	public GameObject player;


	private Transform leftC;
	private Transform rightC;

	private float size1;
	private float size2;
	private float size3;

	private int n_baliza;

	private float current_time;
	private float start_time;

	private float [] times;

	// Start is called before the first frame update
	void Start()
	{
		start_time = current_time = Time.time;
		times = new float[] {0.0f, 0.0f, 0.0f};

		rightC = player.transform.Find("Controller (right)");
		leftC = player.transform.Find("Controller (left)");

		size1 = baliza1.GetComponent<SphereCollider> ().radius;
		size2 = baliza2.GetComponent<SphereCollider> ().radius;
		size3 = baliza3.GetComponent<SphereCollider> ().radius;

		baliza1.SetActive (true);
		baliza2.SetActive (false);
		baliza3.SetActive (false);

		n_baliza = 1;
	}
	void OnEnable()
	{
		start_time = current_time = Time.time;
		times = new float[] {0.0f, 0.0f, 0.0f};

		rightC = player.transform.Find("Controller (right)");
		leftC = player.transform.Find("Controller (left)");

		size1 = baliza1.GetComponent<SphereCollider> ().radius;
		size2 = baliza2.GetComponent<SphereCollider> ().radius;
		size3 = baliza3.GetComponent<SphereCollider> ().radius;

		baliza1.SetActive (true);
		baliza2.SetActive (false);
		baliza3.SetActive (false);

		n_baliza = 1;
	}

	// Update is called once per frame
	void Update()
	{

		if (n_baliza == 1) {
			float distR = Vector3.Distance (rightC.transform.position, baliza1.transform.position);
			float distL = Vector3.Distance (leftC.transform.position, baliza1.transform.position);
			if (distR < size1 || distL < size1) {
				Destroy (baliza1);
				n_baliza = 2;
				baliza2.SetActive (true);
				times [0] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en econtrar la baliza 1 = " + times [0]);
			}
		}
		else if (n_baliza == 2) {
			float distR = Vector3.Distance (rightC.transform.position, baliza2.transform.position);
			float distL = Vector3.Distance (leftC.transform.position, baliza2.transform.position);
			if (distR < size2 || distL < size2) {
				Destroy (baliza2);
				n_baliza = 3;
				baliza3.SetActive (true);
				times [1] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en econtrar la baliza 2 = " + times [1]);
			}
		}
		else if (n_baliza == 3) {
			float distR = Vector3.Distance (rightC.transform.position, baliza3.transform.position);
			float distL = Vector3.Distance (leftC.transform.position, baliza3.transform.position);
			if (distR < size3 || distL < size3) {
				Destroy (baliza3);
				n_baliza = 4;
				times [2] = (Time.time - current_time);
				current_time = Time.time;
				Debug.Log ("Tiempo en econtrar la baliza 3 = " + times [2]);
				Debug.Log ("TIEMPO TOTAL EN ENCONTRAR LAS BALIZAS = " + (Time.time - start_time));
			}
		}

	}

}

